#ifndef BC1_H
#define BC1_H

#include <config.h>
#include <be.h>

#define OPTION(a,b,c,d,e)
#include <arch.h>

#ifndef SETUP
# define SETUP
#endif
#ifndef LOOP_BEGIN_HOOK
# define LOOP_BEGIN_HOOK
#endif
#ifndef LOOP_END_HOOK
# define LOOP_END_HOOK
#endif

#define BF_INSN_P(c) ((c=='+')||(c=='-')||(c=='<')||(c=='>') \
 ||(c=='[')||(c==']')||(c=='.')||(c==',')||(c=='!'))

int optncmp(int, char *, char *);
void help(void);

#endif
