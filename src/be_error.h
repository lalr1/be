#include <config.h>
#if !HAVE_ERROR_AT_LINE
extern void error(int, int, const char *, ...);
extern char *invoc_name;
#else
# include <error.h>
#endif
