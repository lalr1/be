#ifndef BE_H
#define BE_H

#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>

#include <be_error.h>
#include <xmalloc.h>

#if STDC_HEADERS
# include <string.h>
#else
# if !HAVE_STRCHR
#  define strchr index
#  define strrchr rindex
# endif
char *strchr(const char *, int), *strrchr(const char *, int);
char *strdup(const char *);
int strlen(const char *);
int strcmp(const char *, const char *);
char *basename(char *);
#endif


#if !HAVE_SIZE_T
typedef unsigned size_t;
#endif


#define BC1_DIR LIBEXECDIR "/binencephalos"
#define ISDASH(s) (s[0]&&s[0]=='-'&&s[1]=='\0')

#if !HAVE_GETOPT_LONG

struct option {
    char *name;
    int has_arg;
    int *flag;
    int val;
};
# define no_argument 0
# define required_argument 1
# define optional_argument 2

#else

# include <getopt.h>

#endif /* HAVE_GETOPT_LONG */

#define BKEND_MIN_ARGC 5
#define ARG_ASIZE 1
#define ARG_ENTRY 2
#define ARG_FNAME 3
#define ARG_ONAME 4

#endif
