#include <config.h>
#include <be.h>
#include <stdlib.h>

void *xmalloc(size_t s)
{
    void *r=malloc(s);
    if (r)
	return r;
    error(0, errno, "malloc");
    abort();
}

void *xrealloc(void *p, size_t s)
{
    void *r=realloc(p, s);
    if (r)
	return r;
    error(0, errno, "realloc");
    abort();
}

