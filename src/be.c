#include <be.h>
#include <driver.h>

int
main(int argc, char **argv)
{
    int c = 0, hlp = 0;
    int status, infd;
    pid_t pid;
    char *tgt = DEF_TARGET, *outfname = "";
    char *infname="", *asize="", *entry="";
    char **bkend_args=NULL;
    int nargs=BKEND_MIN_ARGC;

#if !HAVE_ERROR_AT_LINE
    invoc_name=argv[0];
#endif

    /* Options */
    bkend_args=xmalloc((BKEND_MIN_ARGC+1)*sizeof(char *));
    while (c>=0) {
	int indx;
	c = GETOPT(argc, argv, SHORTOPTS, opts, &indx);
	switch (c) {
	case 'h' :
	    main_help(0);
	    break;
	case 'H' :
	    hlp=1;
	    break;
	case 'v' :
	    version();
	case 'e' :
	    entry=optarg;
	    break;
	case 'a' :
	    asize=optarg;
	    break;
	case 'b' :
	    bkend_args=xrealloc(bkend_args, (++nargs+1)*sizeof(char *));
	    bkend_args[nargs-1]=optarg;
	    break;
	case 't' :
	    tgt=optarg;
	    break;
	case 'o' :
	    outfname=optarg;
	    break;
	case '?' :
	    error(1, 0, "%s", helpstr);
	}
    }

    if (!tgt)
	error(1, 0, "no target selected, and no default configured.");

    if (hlp)
	backend_help(tgt);

    if (optind >= argc)
	error(1, 0, "no input files\n%s", helpstr);
    if (argv[optind+1])
	error(1, 0, "too many input files\n%s", helpstr);

    bkend_args[ARG_ASIZE]=asize;
    bkend_args[ARG_ENTRY]=entry;
    bkend_args[ARG_ONAME]=outfname;
    bkend_args[nargs]=NULL;

    bkend_args[ARG_FNAME]=infname=argv[optind];
    /* Open input file */
    if (ISDASH(infname)) {
	infd=0;			/* stdin */
    } else {
	infd=open(infname, O_RDONLY);
	if (infd < 0)
	    error(1, errno, "%s", infname);
	dup2(infd, 0);
    }

    /* Run backend. */
    if ((pid=fork())<0)
	error(1, errno, "fork failed");
    if (pid) {
	wait(&status);
	if (!WIFEXITED(status))
	    status=127;
	else
	    status=WEXITSTATUS(status);
    } else {
	exec_backend(tgt, bkend_args);
    }
    exit(status);
}

/* Look for BC1 in standard directories: $BC1DIR,
 * ${libexecdir}/binencephalos and `.'. */
char *
find_bkend_dir(char *bc1)
{
    char *path=BC1_DIR;
    char *edir=getenv("BC1DIR");
    if (try_bkend(edir, bc1))
	return edir;
    else if (try_bkend(path, bc1))
	return path;
    else if (try_bkend(".", bc1))
	return ".";
    else
	error(1, 0, "couldn't find %s", bc1);
}

/* Check that DIR/BC1 exists. We leave permission-checking to exec().*/
int
try_bkend(char *dir, char *bc1)
{
    int f, r;
    char *p;
    if (!dir)
	return 0;
    p=xmalloc(strlen(dir)+strlen(bc1)+2);
    sprintf(p, "%s/%s", dir, bc1);
    if ((f=open(p, O_RDONLY))<0&&errno==ENOENT)
	r=0;
    else
	close(f), r=1;
    free(p);
    return r;
}

/* exec the backend for target TGT. */
void
exec_backend(char *tgt, char **args)
{
    char *bc1=xmalloc(strlen(tgt)+5);
    char *dir, *path, **p;
    sprintf(bc1, "%s-bc1", tgt);
    dir=find_bkend_dir(bc1);
    path=xmalloc(strlen(dir)+strlen(bc1)+2);
    sprintf(path, "%s/%s", dir, bc1);
    args[0]=bc1;
    execv(path, args);
    error(1, errno, "exec: %s", path);
}



int col=0;

char *help_hdr =
"Binencephalos " PACKAGE_VERSION "\n"
"A portable, optimizing, retargetable Brainfuck compiler.\n";

char *help_after =
"Filenames can be `-', meaning stdin or stdout. To see a list of the "
"options defined by the backend for TARGET, use `be -H -t TARGET'.";

char *help_args[1] = { NULL };

void
backend_help(char *tgt)
{
    fputs(help_hdr, stdout);
    exec_backend(tgt, help_args);
}

void
main_help(int exitval)
{
    int i;
    fputs(help_hdr, stdout);
    fputs("Usage: be [options] <input file>\nOptions:\n", stdout);
    for (i=0; opt_doc[i][0]; i++) {
	col=HAVE_GETOPT_LONG ?
	    printf(" -%c, --%s %s ",
		   (char) opts[i].val, opts[i].name, opt_doc[i][0])
	    : printf(" -%c %s ", (char) opts[i].val, opt_doc[i][0]);
	pprint(opt_doc[i][1], OPT_LMARG, 70);
    }
    pprint(help_after, 0, 70);
    puts("This driver was compiled along with the following backends:");
    printf("%s", TARGETS);
    printf(" (default: %s)\n",
	   DEF_TARGET ? DEF_TARGET : "none");
    exit(exitval);
}

void
version()
{
    pprint(
	"Binencephalos "PACKAGE_VERSION"\n"
	"A portable, optimizing, retargetable Brainfuck compiler.\n"
	"Copyright (c) 2005, 2006 Finn Lawler.\n\n"
	"This program is free software; you can redistribute it and/or "
	"modify it under the terms of the GNU General Public License "
	"as published by the Free Software Foundation; either version 2 "
	"of the License, or (at your option) any later version.\n\n"
	"This program is distributed in the hope that it will be useful, "
	"but WITHOUT ANY WARRANTY; without even the implied warranty of "
	"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the "
	"GNU General Public License for more details.\n\n"
	"You should have received a copy of the GNU General Public License along "
	"with this program; if not, write to the Free Software Foundation, Inc., "
	"59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.",
	0, 70);
    exit(0);
}



void
align(int scol)
{
    if (col>=scol)
	putchar('\n'), col=0;
    for ( ; col<scol; col++)
	putchar(' ');
}

void
pprint(char *str, int startcol, int endcol)
{
    if (col!=startcol)
	align(startcol);
    while (*str) {
	int n=0;
	while (str[n]==' ')
	    n++;
	while (str[n]&&str[n]!=' '&&str[n]!='\n')
	    n++;
	if (col+n>endcol&&col>startcol) { /* Next word won't fit */
	    align(startcol);
	    while (*str==' ') /* Skip spaces */
		str++, n--;
	}
	fwrite(str, 1, n, stdout);
	str+=n, col+=n;
	if (*str=='\n')
	    str++, align(startcol);
    }
    align(0);
}
