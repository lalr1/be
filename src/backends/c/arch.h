/* binencephalos definitions for BF-to-C compilation */
#define ARCH_NAME "C"
#define ARCH_DESC "ANSI C"

#define DEF_ANAME "bf_array"
#define DEF_ENTRY "main" 
#define DEF_ASIZE 32768
#define SUFFIX "c"

#define PREAMB \
"\n#include <stdio.h>\n"					\
"\n"								\
""								\
"static unsigned %s %s[%d];\n"	/* cell_type, ANAME, ASIZE */	\
"static int\n"							\
"real_%s (int *trace_hits, char *trace_str)\n" /* FNAME */	\
"{\n"								\
"unsigned %s *p=%s;\n"		/* cell_type, ANAME */	        \
"int c=0;\n"

#define PROLOGUE(asize, aname, fname) \
printf(PREAMB, cell_type, aname, asize, fname, cell_type, aname); \
entry_name=fname; \
array_name=aname

#define READC printf("c=getc(stdin);\n*p = (c == EOF) ? 0 : (char) c;\n")
#define PRINTC printf("putc( (int) *p, stdout);\n")
#define SQ_OPEN(n) printf("while (*p) {\n")
#define SQ_CLOSE(n) printf("}\n")
#define PLUS(n) printf("*p += %d;\n", n)
#define MINUS(n) printf("*p -= %d;\n", n)
#define LEFT(n) printf("p -= %d;\n", n)
#define RIGHT(n) printf("p += %d;\n", n)

#define COMMENT(s) printf("/* %s\n */", s)

static char used_trace=0;
static char *entry_name, *array_name;
#define TRACE(n, pc) \
     used_trace=1;								\
printf("fprintf(stderr, trace_str, %d, %d, trace_hits[%d],(int) (p-%s)/sizeof(%s), *p);\n" \
       "trace_hits[%d]++;\n",	\
       n, pc, n, array_name, cell_type, n)

#define TRACE_STR "tracept %d at line %d:  hit %d, ptr 0x%x [0x%x]\\n"

#define P_TRACE \
"static int trace_hits[%d];\n" \
"static char *trace_str=\"%s\";\n"

#define EPILOGUE \
     printf("return 0;\n}\n\n");				\
if (used_trace)							\
  printf(P_TRACE, traces, TRACE_STR);				\
printf(								\
"int\n"								\
"%s ()\n"							\
"{\n", entry_name);						\
if (used_trace)							\
  printf("real_%s (trace_hits, trace_str);\n", entry_name);	\
  else								\
  printf("real_%s (NULL, NULL);\n", entry_name);		\
printf("return 0;\n");				\
printf("}\n")

static char *cell_type="char";


OPTION("wide-cells", "", no_argument,"Change cell type to `int'.", wide_cells)

#define SETUP					  \
if (wide_cells)				\
   cell_type="int"


