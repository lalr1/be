/* BF-to-assembly definitions for binencephalos (i386-linux) */
#define ARCH_NAME "i386 Linux"
#define ARCH_DESC "i386 assembly (AT&T syntax) with Linux system calls"

/* Defaults and setup */
#ifdef HAVE_ASM_I386_UNISTD_H
#include <asm-i386/unistd.h>
#define SUFFIX "s"
#define INC ""
#else
#define SUFFIX "S"
#define INC "#include <" I386_UNISTD_PATH ">\n"
#endif

#define stringify(x) _xstr(x)
#define _xstr(x) # x

#define DEF_ANAME "bf_array"
#define DEF_ASIZE 32768
#define DEF_ENTRY "_start"

/* Code for simple stuff */
#define READC \
do { used_read=1; printf("\tcall %sread_char\n", sympfx); } while (0)
#define PRINTC \
do { used_print=1; printf("\tcall %sprint_char\n", sympfx); } while (0)
#define SQ_OPEN(n) printf(BEGIN_CODE, n, n)
#define SQ_CLOSE(n) printf(END_CODE, n, n)
#define PLUS(n) printf(PLUS_CODE, cell_suff, n)
#define MINUS(n) printf(MINUS_CODE, cell_suff, n)
#define RIGHT(n) printf(RIGHT_CODE, n*cell_size)
#define LEFT(n) printf(LEFT_CODE, n*cell_size)

#define BEGIN_CODE "%d:\n"\
"\tcmpb $0, (%%eax)\n" \
"\tje %df\n"
#define END_CODE "\tjmp %db\n%d:\n"
#define PLUS_CODE "\tadd%c $%d, (%%eax)\n"
#define MINUS_CODE "\tsub%c $%d, (%%eax)\n"
#define RIGHT_CODE "\taddl $%d, %%eax\n"
#define LEFT_CODE "\tsubl $%d, %%eax\n"

#define COMMENT(s) printf("/* %s */\n", s)

#define PROLOGUE(asize, aname, fname) \
printf(INC PREAMB, aname, asize*cell_size, fname, fname, aname)

#define PREAMB					\
".comm %s, %d\n"				\
"\n"						\
".text\n"					\
"	.globl %s\n"				\
"\n"						\
"%s:\n"					\
"	movl $%s, %%eax\n"			\
"\n"

static char used_read=0, used_print=0, used_trace=0;

#define EPILOGUE							  \
do {									  \
     if (ret)								  \
         printf(DO_RET);						  \
     else								  \
         printf(DO_EXIT);						  \
     if (used_read)							  \
	  printf(READ_SUB, sympfx, READ_SUB_COND);			  \
     if (used_print)							  \
     	  printf(PRINT_SUB, sympfx);					  \
     if (used_trace)							  \
     	  printf(TRACE_SUBS, sympfx, TRC_COND_1, TRC_COND_2, traces*sizeof(int)); \
} while (0)

#define DO_RET "\tret\n"

#define DO_EXIT					\
"\tmov $0, %%ebx\n"				\
"\tmov $"					\
stringify(__NR_exit)				\
", %%eax\n"					\
"\tint $0x80\n"					\
"\n"

/* Code for slightly trickier stuff */

/* This still works with wide cells, because ia32 is little-endian */
#define PRINT_SUB \
"%sprint_char:\n"				\
"\tmov $1, %%ebx\n"				\
"\tmov %%eax, %%ecx\n"				\
"\tmov $1, %%edx\n"				\
"\tpush %%eax\n"				\
"\tmov $"					\
stringify(__NR_write)				\
", %%eax\n"					\
"\tint $0x80\n"					\
"\tpop %%eax\n"					\
"\tret\n"					\
"\n"

#define READ_SUB_COND \
cell_size==1?"":"\tmovl $0, (%eax)\n"

#define READ_SUB				\
"%sread_char:\n"				\
"%s"						\
"\tpushl %%eax\n"				\
"\tmovl $0, %%ebx\n"				\
"\tmovl $1, %%edx\n"				\
"\tpushl $0\n"					\
"\tmovl %%esp, %%ecx\n"				\
"\tmovl $"					\
stringify(__NR_read)				\
" ,%%eax\n"					\
"\tint $0x80\n"					\
"\tpopl %%ebx\n"				\
"\tpopl %%eax\n"				\
"\tmovb %%bl, (%%eax)\n"			\
"\tret\n"

/* Traces */
#define TRACE_CODE				\
"\tmovl $%d, %%edi\n"				\
"\tmovl $%d, %%esi\n"				\
"\tcall %sdo_trace\n"

#define TRACE(n, pc) \
do { used_trace=1; printf(TRACE_CODE, n, pc, sympfx); } while (0)

/* Print number in %ebx to stderr. Uses prints below. */
#define PRINTX					\
"%1$sprintx:\n"					\
"\tmovl	%%esp, %%ebp\n"				\
"\tsubl	$8, %%esp\n"				\
"\tmovl	$8, %%ecx\n"				\
"0:\tdecl	%%ecx\n"			\
"\tjs	0f\n"					\
"\tmovl	%%ebx, %%edx\n"				\
"\tandl	$15, %%edx\n"				\
"\tmovb	%1$shexits(%%edx), %%dl\n"		\
"\tmovb	%%dl, (%%esp,%%ecx)\n"			\
"\tshrl	$4, %%ebx\n"				\
"\tjnz	0b\n"					\
"0:\tmovl	$8, %%edx\n"			\
"\tsubl	%%ecx, %%edx\n"				\
"\taddl	%%esp, %%ecx\n"				\
"\tcall	%1$sprints\n"				\
"\tmovl	%%ebp, %%esp\n"				\
"\tret\n"

#define PRINTS					\
"%1$sprints:\n"					\
"\tmovl $"					\
stringify(__NR_write)				\
", %%eax\n"					\
"\tmovl $2, %%ebx\n"				\
"\tint $0x80\n"					\
"\tret\n"					\
"\n"

#define DO_TRACE				\
"%1$sdo_trace:\n"					\
"\tpush %%eax\n"				\
"\tmovl $%1$str0, %%ecx\n"			\
"\tmovl $%1$sln0, %%edx\n"			\
"\tcall %1$sprints\n"				\
"\tmovl %%edi, %%ebx\n"				\
"\tcall %1$sprintx\n"				\
"\tmovl $%1$str1, %%ecx\n"			\
"\tmovl $%1$sln1, %%edx\n"			\
"\tcall %1$sprints\n"				\
"\tmovl %%esi, %%ebx\n"				\
"\tcall %1$sprintx\n"				\
"\tmovl $%1$str2, %%ecx\n"			\
"\tmovl $%1$sln2, %%edx\n"			\
"\tcall %1$sprints\n"				\
"\tleal trace_hits(,%%edi,4), %%ebx\n"		\
"\tincl (%%ebx)\n"				\
"\tmovl (%%ebx), %%ebx\n"			\
"\tcall %1$sprintx\n"				\
"\tmovl $%1$str3, %%ecx\n"			\
"\tmovl $%1$sln3, %%edx\n"			\
"\tcall %1$sprints\n"				\
"\tmovl (%%esp), %%ebx\n"			\
"\tsubl $bf_array, %%ebx\n"			\
"%2$s"						\
"\tcall %1$sprintx\n"				\
"\tmovl $%1$str4, %%ecx\n"			\
"\tmovl $%1$sln4, %%edx\n"			\
"\tcall %1$sprints\n"				\
"\tmovl (%%esp), %%ebx\n"			\
"%3$s"						\
"\tcall %1$sprintx\n"				\
"\tmovl $%1$str5, %%ecx\n"			\
"\tmovl $%1$sln5, %%edx\n"			\
"\tcall %1$sprints\n"				\
"\tpop %%eax\n"					\
"\tret\n"					\
".data\n"					\
"%1$shexits:\n"					\
"\t.ascii \"0123456789abcdef\"\n"		\
"%1$str0:	.ascii \"tracept 0x\"\n"		\
".set %1$sln0, .-%1$str0\n"				\
"%1$str1:	.ascii \" at line 0x\"\n"		\
"\t.set %1$sln1, .-%1$str1\n"			\
"%1$str2:	.ascii \": hit 0x\"\n"			\
"\t.set %1$sln2, .-%1$str2\n"			\
"%1$str3:	.ascii \", ptr 0x\"\n"			\
"\t.set %1$sln3, .-%1$str3\n"			\
"%1$str4:	.ascii \" [0x\"\n"			\
"\t.set %1$sln4, .-%1$str4\n"			\
"%1$str5:	.ascii \"]\\n\"\n"			\
"\t.set %1$sln5, .-%1$str5\n"			\
".comm trace_hits, %4$d\n"			\
".text\n"

#define TRC_COND_1 \
cell_size==1?"":"\tsarl $2, %ebx\n"
#define TRC_COND_2 \
cell_size==1?"\tmovzbl (%ebx), %ebx\n":"\tmovl (%ebx), %ebx\n"

#define TRACE_SUBS				\
PRINTX						\
PRINTS						\
DO_TRACE

static int cell_suff='b';
static int cell_size=1;
static int ret=0;
static char *sympfx="";

OPTION("wide-cells", "", no_argument, "Use 4-byte cells.", wide_cells)

OPTION("return", "", no_argument,
       "Make output code finish with `ret' instead of calling exit(2).", retp)

OPTION("symbol-prefix", "[=STRING]", optional_argument,
"Prepend STRING to all symbols except entry point (default \".L\").", symp)

#define SETUP					\
if (wide_cells)					\
    cell_suff='l', cell_size=4;			\
if (retp)					\
    ret=1;					\
if (symp) sympfx=*symp ? symp : ".L"
