.Lprintx:
	movl	%esp, %ebp
	subl	$8, %esp
	movl	$8, %ecx
0:	decl	%ecx
	js	0f
	movl	%ebx, %edx
	andl	$15, %edx
	movb	.Lhexits(%edx), %dl
	movb	%dl, (%esp,%ecx)
	shrl	$4, %ebx
	jnz	0b
0:	movl	$8, %edx
	subl	%ecx, %edx
	addl	%esp, %ecx
	call	.Lprints
	movl	%ebp, %esp
	ret
