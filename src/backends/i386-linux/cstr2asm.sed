# trim end of line
s/[[:space:]]*\\$//g
# remove unescaped `"'
s/\([^\]\)"/\1/g
# and at bol
s/^"//
# won't work for `"' embedded in asm string constant
s/\\"/"/g
# remove/replace C escapes
s/\\n$//
s/^\\t/	/
# do register names
s/%%/%/g
# not perfect, leaves macro on its own line
s/^stringify(\([A-Za-z_]\+\))/\1/