# remove comments
s/[[:space:]]*;.*$//
# double backslashes
s,\\,\\\\,g
# convert leading space to tab, preserve local labels
s/^\([0-9]\+:\)\?[[:space:]]\+/\1\\t/
# quote `"'
s/\([^\]\)"/\1\\"/g
# open string at bol
s/^/"/
# add newline and close string at eol
s/$/\\n"/
# replace unquoted % with %%
s/\([^\\]\)%/\1%%/g
s/\\%/%/g
