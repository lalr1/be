#include <be.h>
#include <xmalloc.h>

#define BLKSZ 64
#define ASIZE 32768
#define BF_INSN_P(c) ((c=='+')||(c=='-')||(c=='<')||(c=='>') \
 ||(c=='[')||(c==']')||(c=='.')||(c==',')||(c=='!'))

/* BF instructions */
typedef enum {
    PLUS=0,
    MINUS,
    LEFT,
    RIGHT,
    READ,
    WRITE,
    /* These next 6 instructions have a following int, giving count or
     * jump address. */
    OPEN,
    CLOSE,
    PLUS_C,			/* Repeat-count forms of instructions */
    MINUS_C,
    LEFT_C,
    RIGHT_C
} bf_opcode;

/* BF program's array */
unsigned char *bfarray;
/* Holds compiled form of BF program */
unsigned char *prog;
/* Holds loop-start addresses during compilation */
int *loops;

int main(int argc, char **argv)
{
    if (argc<2||argc>3) {
	puts("usage: bei <file> [array size]");
	exit(1);
    }
    FILE *f=NULL;
    if (argv[1][0]=='-'&&argv[1][1]=='\0')
	f=stdin;
    else
	f=fopen(argv[1], "r");
    if (!f) {
	perror("open");
	exit(1);
    }
    int asize= argc==3 ? atoi(argv[2]) : ASIZE;
    bfarray=xmalloc(asize);
    /* 2 chars and instruction counter */
    int c, d, i=0;
    /* Repeat counter, bytes allocated, line and column count */
    int rep, alloc=0, line=1, col=0;
    int nloops=0, lalloc=0;
    while ((c=getc(f))!=EOF) {
	col++;
	if (i>alloc-(signed)(1+sizeof(int)))
	    prog=xrealloc((void *)prog, alloc+=BLKSZ);
	rep=0;
	if (c=='+'||c=='-'||c=='<'||c=='>') {
	    for (rep++, d=getc(f); d!=EOF; d=getc(f))
		if (d==c)
		    rep++, col++;
		else if (BF_INSN_P(d))
		    break;
		else if (d=='\n')
		    line++, col=0;
	    ungetc(d, f);
	}
	switch (c) {
	case '\n' :
	    col=0; line++;
	    break;
	case '+' :
	    prog[i++]=rep>1 ? PLUS_C : PLUS;
	    break;
	case '-' :
	    prog[i++]=rep>1 ? MINUS_C : MINUS;
	    break;
	case '<' :
	    prog[i++]=rep>1 ? LEFT_C : LEFT;
	    break;
	case '>' :
	    prog[i++]=rep>1 ? RIGHT_C : RIGHT;
	    break;
	case '.' :
	    prog[i++]=WRITE;
	    break;
	case ',' :
	    prog[i++]=READ;
	    break;
	case '[' :
	    if (lalloc<=nloops)
		loops=xrealloc(loops, (lalloc+=8)*sizeof(int));
	    loops[nloops++]=i+1;
	    prog[i++]=OPEN;
	    i+=sizeof(int);	/* space for later patch */
	    break;
	case ']' : {
	    int patch;
	    if (nloops==0) {
		fprintf(stderr, "%s:%d,%d: syntax error: unbalanced `]'\n",
			argv[1], line, col);
		exit(1);
	    }
	    prog[i++]=CLOSE;
	    /* patch jump addresses */
	    patch=loops[--nloops];
	    *(int *)(prog+patch)=i+sizeof(int);
	    *(int *)(prog+i)=patch+sizeof(int);
	    i+=sizeof(int);
	    break;
	}
	}
	if (c=='+'||c=='-'||c=='<'||c=='>') {
	    if (rep>1) {
		*(int *)(prog+i)=rep;
		i+=sizeof(int);
	    }
	}
    }
    if (nloops) {
	fprintf(stderr, "%s:%d,%d: syntax error: unbalanced `['\n",
		argv[1], line, col);
	exit(1);
    }
    free(loops);
    fclose(f);
    int pc=0, ptr=0;
    while (pc<i) {
	c=prog[pc++];
	switch (c) {
	case PLUS : bfarray[ptr]++; break;
	case PLUS_C :
	    bfarray[ptr] += *(int *)(prog+pc);
	    break;
	case MINUS : bfarray[ptr]--; break;
	case MINUS_C :
	    bfarray[ptr] -= *(int *)(prog+pc);
	    break;
	case LEFT : ptr--; break;
	case LEFT_C :
	    ptr -= *(int *)(prog+pc);
	    break;
	case RIGHT : ptr++; break;
	case RIGHT_C :
	    ptr += *(int *)(prog+pc);
	    break;
	case READ :
	    if ((d=getchar())==EOF)
		d=0;
	    bfarray[ptr]=(unsigned char) d;
	    break;
	case WRITE :
	    putchar((int) bfarray[ptr]);
	    break;
	case OPEN :
	    if (bfarray[ptr]==0) {
		pc = *(int *)(prog+pc);
		continue;
	    }
	    break;
	case CLOSE :
	    /* optimize slightly by skipping backward jump if *ptr!=0 */
	    if (bfarray[ptr]!=0) {
		pc = *(int *)(prog+pc);
		continue;
	    }
	    break;
	}
	if (c>WRITE) 
	    pc+=sizeof(int);
	if (ptr<0||ptr>=asize) {
	    fprintf(stderr, "array index out of bounds: %d\n", ptr);
	    exit(0);
	}
    }
    return 0;
}
