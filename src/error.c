#include <config.h>
#include <stdio.h>
#if HAVE_STDARG_H
# include <stdarg.h>
#else
# include <varargs.h>
#endif

#include <errno.h>

char *invoc_name;

#if HAVE_STDARG_H
void error(int status, int err, const char *fmt, ...)
#else
void error(status, err, fmt, va_alist)
  int status, err;
  const char *fmt;
  va_dcl
#endif
{
    va_list a;
#if HAVE_STDARG_H
    va_start(&a, fmt);
#else
    va_start(a);
#endif
    fprintf(stderr, "%s: ", invoc_name);
    vfprintf(stderr, fmt, a);
    if (err) {
	int e;
	fprintf(stderr, "%s", ": ");
	e=errno;
	errno=err;
	perror(NULL);
	errno=e;
    } else {
	fputc('\n', stderr);
    }
    va_end(a);
    if (status)
	exit(status);
}
    
