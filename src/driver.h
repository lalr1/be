#ifndef DRIVER_H
#define DRIVER_H
#include <config.h>

#if HAVE_SYS_WAIT_H
# include <sys/wait.h>
#endif
#ifndef WIFEXITED
# define WIFEXITED(s) (((s)&255)==0)
#endif
#ifndef WEXITSTATUS
# define WEXITSTATUS(s) ((unsigned) (s) >> 8)
#endif

#if !HAVE_PID_T
typedef int pid_t;
#endif

#if !HAVE_GETOPT_LONG
char *helpstr="Try `be -h' for more information";
# define GETOPT(c, v, s, o, i) getopt(c, v, s)
# define OPT_LMARG 15
#else
# define GETOPT getopt_long
# define OPT_LMARG 30
char *helpstr="Try `be --help' for more information";
#endif

#define SHORTOPTS "hHvt:o:b:e:a:"
struct option opts[] =
{
    { "help", no_argument, NULL, 'h' },
    { "backend-help", no_argument, NULL, 'H' },
    { "version", no_argument, NULL, 'v' },
    { "target", required_argument, NULL, 't' },
    { "output", required_argument, NULL, 'o' },
    { "backend-option", required_argument, NULL, 'b' },
    { "entry", required_argument, NULL, 'e' },
    { "array-size", required_argument, NULL, 'a' },
    { 0, 0, 0, 0 }
};

char *opt_doc[][2] =
{
    { "", "Show this help." },
    { "", "Show help for selected or default target." },
    { "", "Show information on version, licence and warranty." },
    { "TARGET", "Set target architecture for generated code." },
    { "FILE", "Set name of output file." },
    { "OPTION",
      "Pass option OPTION to the backend. Multiple uses accumulate." },
    { "NAME", "Set name of entry point to generated code (e.g. `main')." },
    { "SIZE", "Set size (in cells) of BF program's array." },
    { 0, 0 }
};


char *find_bkend_dir(char *);
int try_bkend(char *, char *);
void exec_backend(char *, char **);
void usage(void);
void version(void);
void backend_help(char *);
void main_help(int);
void align(int);
void pprint(char *, int, int);

#endif /* DRIVER_H */
